#!/bin/bash

ENVIRONMENT_VERSION=$(aws ssm get-parameter --region=us-east-1 --name "/PAAS/REPO/ENVIRONMENT_VERSION" | jq -r '.Parameter.Value')
ENVIRONMENT_REPO=/TechCrumble-PaaS-Environments
ENVIRONMENT_REPO_URL=https://github.com/ArunaLakmal/TechCrumble-PaaS-Environments.git
INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)
ROLE=$(aws ec2 describe-tags --region=us-east-1 --filters "Name=resource-id,Values=${INSTANCE_ID}" "Name=key,Values=Name" --output=text | cut -f5)
MASTER_REPO=ansible-kube-master
WORKER_REPO=ansible-kubernetes-worker
ETCD_REPO=terraform-kube-etcd
MASTER_REPO_URL=https://github.com/ArunaLakmal/ansible-kubernetes-master.git
WORKER_REPO_URL=https://github.com/ArunaLakmal/ansible-kubernetes-worker.git
ETCD_REPO_URL=https://github.com/ArunaLakmal/ansible-kubernetes-etcd.git
ROOT=/

run_ansible() {
  git clone "$ENVIRONMENT_REPO_URL" --branch "$ENVIRONMENT_VERSION" --single-branch
  cd "$ENVIRONMENT_REPO"
  MASTER_VERSION=$(jq -r '.ansible_kubernetes_master_version' terraform.tfvars.json)
  WORKER_VERSION=$(jq -r '.ansible_kubernetes_worker_version' terraform.tfvars.json)
  ETCD_VERSION=$(jq -r '.ansible_kubernetes_etcd_version' terraform.tfvars.json)
  if [[ "$ROLE" == "kube_master" ]]; then
    git clone "$MASTER_REPO_URL" --branch "$MASTER_VERSION" --single-branch
    make master
  elif [[ "$ROLE" == "kube_worker" ]]; then
    git clone "$WORKER_REPO_URL" --branch "$WORKER_VERSION" --single-branch
    make worker
  elif [[ "$ROLE" == "kube_etcd" ]]; then
    git clone "$ETCD_REPO_URL" --branch "$ETCD_VERSION" --single-branch
    make etcd
  else
    exit 0
  fi
}

cd  "$ROOT"
if [[ -d "$ENVIRONMENT_REPO" ]]; then
  rm -rf "$ENVIRONMENT_REPO"
  run_ansible
else
  run_ansible
fi